#include <exception>
#include <iostream>
#include <array>
#include <cstdlib>
#include <ctime>
#include <random>

#include "glm.hpp"
#include "Renderer.h"

using namespace Ogre;
using namespace OgreBites;
using namespace ProcTerr;

Renderer::Renderer() : _isLeftMousePressed(false), _isRightMousePressed(false),
	_lookPoint(static_cast<float>(SIZE*0.5f), 0.0f, static_cast<float>(SIZE*0.5f))
{
	std::unique_ptr<std::array<std::array<Vector2, NUMSQRS + 2>, NUMSQRS + 2>> grid(
		std::make_unique<std::array<std::array<Vector2, NUMSQRS + 2>, NUMSQRS + 2>>());


	for (size_t y = 0; y < NUMSQRS + 2; ++y)
	{
		for (size_t x = 0; x < NUMSQRS + 2; ++x)
		{
			(*grid)[x][y] = Vector2(static_cast<float>(rand() * RAND_MAX - RAND_MAX),
				static_cast<float>(rand() * RAND_MAX - RAND_MAX)).normalisedCopy();
		}
	}
	_grid = move(grid);
}

Renderer::~Renderer()
{
}

bool Renderer::getEndRendering() const
{
	return _endRendering;
}

float Renderer::getDeltaTime() const
{
	return _deltaTime;
}

void Renderer::setEndRendering(bool isEnd)
{
	_endRendering = isEnd;
}

void Renderer::setDeltaTime(float delta)
{
	_deltaTime = delta;
}

// Setup is called in OgreApplicationContextBase.cpp at line 54
void Renderer::setup()
{

	// Base context.
	ApplicationContext::setup();
	addInputListener(this);

	// Get pointer to root.
	Root* pRoot = getRoot();
	pScnMngr = pRoot->createSceneManager();

	// Register this scene to shader generator(RTSS).
	RTShader::ShaderGenerator* pShaderGen = RTShader::ShaderGenerator::getSingletonPtr(); //static method, does not need instance.
	pShaderGen->addSceneManager(pScnMngr);

	// Set ambient light.
	pScnMngr->setAmbientLight(ColourValue(0.2f, 0.2f, 0.2f));

	// Set directional light.
	Vector3 dirLight(SIZE * 0.5f, MAX_HEIGHT * 1.5f, SIZE * 0.5f);
	dirLight.normalise();
	Light* pDirLight = pScnMngr->createLight("DirectionalLight");
	pDirLight->setType(Light::LT_DIRECTIONAL);
	pDirLight->setDirection(dirLight);
	pDirLight->setDiffuseColour(ColourValue::White);
	pDirLight->setSpecularColour(Ogre::ColourValue(0.4f, 0.4f, 0.4f));

	// Create camera.
	_pCam = pScnMngr->createCamera("MainCam");
	_pCamNode = pScnMngr->getRootSceneNode()->createChildSceneNode();
	_pCam->setNearClipDistance(0.1f); // specific to this sample
	_pCam->setOrientation(Quaternion());
	_pCamNode->attachObject(_pCam);
	_pCamNode->setPosition(Vector3(0.0f, 0.0f,0.0f));
	_lookPoint.normalise();
	_pCamNode->lookAt(_lookPoint, Node::TS_PARENT);

	// Tell it to render into the main window.
	Viewport * vp = getRenderWindow()->addViewport(_pCam);
	vp->setBackgroundColour(ColourValue(0.9f, 0.6f, 0.5f));
	_pCam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));

	// Load mesh
	// Create the mesh via the MeshManager
	MeshPtr mesh = MeshManager::getSingleton().createManual(NAME, "General");

	// Create one submesh
	SubMesh* pSubMesh = mesh->createSubMesh();

	_CreateMesh(mesh, pSubMesh);

	Entity* pTerrain = pScnMngr->createEntity(NAME);
	SceneNode* thisSceneNode = pScnMngr->getRootSceneNode()->createChildSceneNode();
	thisSceneNode->setPosition(0, 0, 0);
	thisSceneNode->attachObject(pTerrain);
}

void Renderer::_CountNormals(const std::unique_ptr < std::array<float, (NUMSQRS + 1) * (NUMSQRS + 1) * 6>> & pVertexArray, 
	const std::unique_ptr < std::array<unsigned short, NUMTR * 3>>& pTriangleArray)
{
	// Count normal vectors for vertices:
	// 1. Count cross products of triangles.
	std::unique_ptr<std::array<glm::tvec3<float>, NUMTR>> pArrayOfFaceNormals(
		std::make_unique<std::array<glm::tvec3<float>, NUMTR>>());;
	for (size_t i = 0; i < NUMTR; ++i)
	{
		size_t firstVertex = (*pTriangleArray)[i * 3];
		size_t secondVertex = (*pTriangleArray)[i * 3 + 1];
		glm::tvec3<float> firstVector((*pVertexArray)[firstVertex * 6], (*pVertexArray)[firstVertex * 6 + 1], (*pVertexArray)[firstVertex * 6 + 2]);
		glm::tvec3<float> secondVector((*pVertexArray)[secondVertex * 6], (*pVertexArray)[secondVertex * 6 + 1], (*pVertexArray)[secondVertex * 6 + 2]);

		(*pArrayOfFaceNormals)[i] = normalize(glm::cross(normalize(firstVector), normalize(secondVector)));
	}

	// 2. Count vertex normals
	glm::tvec3<float> additionVec(0.0f,0.0f,0.0f);
	size_t squaresInLine = ((NUMSQRS + 1) - 1) * 2;
	size_t minusOne = 0 - 1;

	for (size_t i = 0; i < (NUMSQRS + 1); ++i)
	{
		for (size_t j = 0; j < (NUMSQRS + 1); ++j)
		{
			size_t numOfVertex = i * (NUMSQRS + 1) + j;
			if ((i - 1 != minusOne) && (j - 1 != minusOne))
			{
				additionVec += (*pArrayOfFaceNormals)[squaresInLine * (i - 1) + j * 2 - 2];
				additionVec += (*pArrayOfFaceNormals)[squaresInLine * (i - 1) + j * 2 - 1];
			}
			if ((j - 1 != minusOne) && (i + 1 < (NUMSQRS + 1)) )
			{
				additionVec += (*pArrayOfFaceNormals)[squaresInLine * i + j * 2 - 1];
			}
			if ((i + 1 < (NUMSQRS + 1)) && (j + 1 < (NUMSQRS + 1)))
			{
				additionVec += (*pArrayOfFaceNormals)[squaresInLine * i + j * 2 + 1];
				additionVec += (*pArrayOfFaceNormals)[squaresInLine * i + j * 2];
			}
			if ((j + 1 < (NUMSQRS + 1)) && (i - 1 != minusOne))
			{
				additionVec += (*pArrayOfFaceNormals)[squaresInLine * (i - 1) + j * 2 + 1];
			}

			additionVec = glm::normalize(additionVec);
			(*pVertexArray)[numOfVertex * 6 + 3] = additionVec.x;
			(*pVertexArray)[numOfVertex * 6 + 4] = additionVec.y;
			(*pVertexArray)[numOfVertex * 6 + 5] = additionVec.z;

		}
	}
}

float Renderer::_GetRandomFloat(float from, float to) const
{
	// Create random generator
	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_real_distribution<float> randFromDistribution(from, to);

	return randFromDistribution(rng);
}

void Renderer::_CreateMesh(MeshPtr & mesh, SubMesh* pSubMesh)
{

	// Diamond-square algorithm 
	//float MAX_HEIGHT = 20.0f;
	std::unique_ptr <std::array<float, (NUMSQRS + 1) * (NUMSQRS + 1)>> pArrayOfHeights(
		std::make_unique<std::array<float, (NUMSQRS + 1) * (NUMSQRS + 1) >>());
	srand(static_cast <unsigned int> (time(0)));

	_DiamondSquare(pArrayOfHeights, MAX_HEIGHT);


	// Fill vertex and triangle array
	std::unique_ptr < std::array<unsigned short, NUMTR * 3>> pTriangleArray(std::make_unique <std::array<unsigned short, NUMTR * 3>>());
	size_t indexOfTriangles = 0;

	std::unique_ptr < std::array<float, (NUMSQRS + 1) * (NUMSQRS + 1) * 6>> pVertexArray(std::make_unique<std::array<float, (NUMSQRS + 1) * (NUMSQRS + 1) * 6>>());
	size_t index = 0;
	float xoff = _GetRandomFloat(0.0f, 9999.0f);
	float yoff = _GetRandomFloat(0.0f, 9999.0f);
	int scale = 3;

	for (size_t i = 0; i < (NUMSQRS + 1); ++i)
	{
		for (size_t j = 0; j < (NUMSQRS + 1); ++j)
		{
			// Map x and y coordinate to unit square and scale perlin noise value and add random value.
			float xCoord = static_cast<float>(i) / (NUMSQRS + 1) * scale + xoff;
			float yCoord = static_cast<float>(j) / (NUMSQRS + 1) * scale + yoff;

			auto a = _OctavePerlin(xCoord, yCoord, 6, 0.35f);
			float mappedA = -MAX_HEIGHT + (2 * MAX_HEIGHT) * a;
			// vertex vector
			(*pVertexArray)[index] = static_cast<float>(j * (SIZE - 1)/NUMSQRS); // x coordinate
			// Y coordinate(height) is combined from perlin noise and diamond-square algorithm
			(*pVertexArray)[index + 1] = static_cast<float>(mappedA +
										0.25*(*pArrayOfHeights)[i*(NUMSQRS + 1) + j]); // y coordinate
			(*pVertexArray)[index + 2] = static_cast<float>(i * (SIZE - 1) / NUMSQRS); // z coordinate


			if (i < NUMSQRS && j < NUMSQRS)
			{
				// First vertex of first triangle
				(*pTriangleArray)[indexOfTriangles] = static_cast<unsigned short>((i + 1) * (NUMSQRS + 1) + j + 1);
				// Second vertex of first triangle
				(*pTriangleArray)[indexOfTriangles + 1] = static_cast<unsigned short>(i * (NUMSQRS + 1) + j + 1);
				// Third vertex of first triangle
				(*pTriangleArray)[indexOfTriangles + 2] = static_cast<unsigned short>(i * (NUMSQRS + 1) + j);

				// First vertex of second triangle
				(*pTriangleArray)[indexOfTriangles + 3] = static_cast<unsigned short>(i * (NUMSQRS + 1) + j);
				// Second vertex of second triangle
				(*pTriangleArray)[indexOfTriangles + 4] = static_cast<unsigned short>((i + 1) * (NUMSQRS + 1) + j);
				// Third vertex of second triangle
				(*pTriangleArray)[indexOfTriangles + 5] = static_cast<unsigned short>((i + 1) * (NUMSQRS + 1) + j + 1);

				indexOfTriangles += 6;
			}
			index += 6;
		}

	}
	_CountNormals(pVertexArray, pTriangleArray);
	_pCamNode->setPosition(0.0f, (*pVertexArray)[1] + MAX_HEIGHT, 0.0f);
	_pCamNode->lookAt(Vector3(SIZE * 0.5, (*pVertexArray)[1], SIZE * 0.5), Node::TS_WORLD);

	// Create vertex data structure for WIDTH * HEIGHT vertices shared between submeshes
	mesh->sharedVertexData = new VertexData();
	mesh->sharedVertexData->vertexCount = (NUMSQRS + 1) * (NUMSQRS + 1);

	// Create declaration (memory format) of vertex data
	VertexDeclaration* pVDecl = mesh->sharedVertexData->vertexDeclaration;
	size_t offset = 0;

	// 1st buffer (for vertexArray)
	pVDecl->addElement(0, offset, VET_FLOAT3, VES_POSITION);
	offset += VertexElement::getTypeSize(VET_FLOAT3);
	pVDecl->addElement(0, offset, VET_FLOAT3, VES_NORMAL);
	offset += VertexElement::getTypeSize(VET_FLOAT3);

	// Allocate vertex buffer of the requested number of vertices (vertexCount) 
	// and bytes per vertex (offset)
	HardwareVertexBufferSharedPtr pVertexBuffer =
		HardwareBufferManager::getSingleton().createVertexBuffer(
			offset, mesh->sharedVertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY);

	// Upload the vertex data to the card
	pVertexBuffer->writeData(0, pVertexBuffer->getSizeInBytes(), pVertexArray->data(), true);

	// Set vertex buffer binding so buffer 1 is bound to our vertex buffer
	VertexBufferBinding* bind = mesh->sharedVertexData->vertexBufferBinding;
	bind->setBinding(0, pVertexBuffer);

	// 3rd buffer (for triangleArray)
	// Allocate index buffer of the requested number of vertices (ibufCount) 
	HardwareIndexBufferSharedPtr pIndexBuffer = HardwareBufferManager::getSingleton().
	createIndexBuffer(
	HardwareIndexBuffer::IT_16BIT,
	NUMTR * 3,
	HardwareBuffer::HBU_STATIC_WRITE_ONLY);

	// Upload the index data to the card
	pIndexBuffer->writeData(0, pIndexBuffer->getSizeInBytes(), pTriangleArray->data(), true);

	// Set parameters of the submesh
	pSubMesh->useSharedVertices = true;
	pSubMesh->indexData->indexBuffer = pIndexBuffer;
	pSubMesh->indexData->indexCount = NUMTR * 3;
	pSubMesh->indexData->indexStart = 0;

	// Set bounding information (for culling)
	mesh->_setBounds(AxisAlignedBox(0, 0, 0, SIZE, 0, SIZE));
	mesh->_setBoundingSphereRadius(0);

	// Notify -Mesh object that it has been loaded
	mesh->load();
}


float Renderer::_Grad(int hash, float x, float y, float z)
{
	switch (hash & 0xF)
	{
	case 0x0: return  x + y;
	case 0x1: return -x + y;
	case 0x2: return  x - y;
	case 0x3: return -x - y;
	case 0x4: return  x + z;
	case 0x5: return -x + z;
	case 0x6: return  x - z;
	case 0x7: return -x - z;
	case 0x8: return  y + z;
	case 0x9: return -y + z;
	case 0xA: return  y - z;
	case 0xB: return -y - z;
	case 0xC: return  y + x;
	case 0xD: return -y + z;
	case 0xE: return  y - x;
	case 0xF: return -y - z;
	default: return 0; // never happens
	}
}

float Renderer::_Lerp(float a, float b, float x)
{
	return a + x * (b - a);
}

float Renderer::_OctavePerlin(float x, float y, int octaves, float persistence)
{
	float total = 0;
	float frequency = 1;
	float amplitude = 1;
	float maxValue = 0;  // Used for normalizing result to 0.0 - 1.0
	for (int i = 0; i < octaves; i++)
	{
		total += _Perlin(x * frequency, y * frequency, 0) * amplitude;

		maxValue += amplitude;

		amplitude *= persistence;
		frequency *= 2;
	}

	return total / maxValue;
}

// Compute Perlin noise at coordinates x, y
float Renderer::_Perlin(float x, float y, float z)
{
	std::unique_ptr<std::array<int, 256>> permutation(std::make_unique < std::array<int, 256>>());
	// Hash lookup table as defined by Ken Perlin.  This is a randomly
	// arranged array of all numbers from 0-255 inclusive
	(*permutation) = { 151,160,137,91,90,15,                
	131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,    
	190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
	88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
	77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
	102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
	135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
	5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
	223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
	129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
	251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
	49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
	138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
	};

	// Doubled permutation to avoid overflow
	std::unique_ptr<std::array<int, 512>> p(std::make_unique < std::array<int, 512>>());
	for (int x = 0; x < 512; x++)
	{
		(*p)[x] = (*permutation)[x % 256];
	}

	int xi = (int)x & 255;                              // Calculate the "unit cube" that the point asked will be located in
	int yi = (int)y & 255;                              // The left bound is ( |_x_|,|_y_| ) and the right bound is that
	int zi = (int)z & 255;
	float xf = x - (int)x;
	float yf = y - (int)y;
	float zf = z - (int)z;

	// Fade function.
	float u = xf * xf * xf * (xf * (xf * 6 - 15) + 10);
	float v = yf * yf * yf * (yf * (yf * 6 - 15) + 10);
	float w = zf * zf * zf * (zf * (zf * 6 - 15) + 10);

	// Hash function.
	int aaa, aba, aab, abb, baa, bba, bab, bbb;
	aaa = (*p)[(*p)[(*p)[xi] + yi] + zi];
	aba = (*p)[(*p)[(*p)[xi] + yi+1] + zi];
	aab = (*p)[(*p)[(*p)[xi] + yi] + zi+1];
	abb = (*p)[(*p)[(*p)[xi] + yi+1] + zi+1];
	baa = (*p)[(*p)[(*p)[xi+1] + yi] + zi];
	bba = (*p)[(*p)[(*p)[xi + 1] + yi+1] + zi];
	bab = (*p)[(*p)[(*p)[xi + 1] + yi] + zi+1];
	bbb = (*p)[(*p)[(*p)[xi + 1] + yi+1] + zi+1];

	float x1, x2, y1, y2;
	x1 = _Lerp(_Grad(aaa, xf, yf, zf),           // The gradient function calculates the dot product between a pseudorandom
		_Grad(baa, xf - 1, yf, zf),             // gradient vector and the vector from the input coordinate to the 8
		u);                                     // surrounding points in its unit cube.
	x2 = _Lerp(_Grad(aba, xf, yf - 1, zf),           // This is all then lerped together as a sort of weighted average based on the faded (u,v,w)
		_Grad(bba, xf - 1, yf - 1, zf),             // values we made earlier.
		u);
	y1 = _Lerp(x1, x2, v);

	x1 = _Lerp(_Grad(aab, xf, yf, zf - 1),
		_Grad(bab, xf - 1, yf, zf - 1),
		u);
	x2 = _Lerp(_Grad(abb, xf, yf - 1, zf - 1),
		_Grad(bbb, xf - 1, yf - 1, zf - 1),
		u);
	y2 = _Lerp(x1, x2, v);

	return (_Lerp(y1, y2, w) + 1) / 2;                      // For convenience we bind the result to 0 - 1 (theoretical min/max before is [-1, 1])
}

void Renderer::_DiamondSquare(std::unique_ptr<std::array<float, (NUMSQRS + 1) * (NUMSQRS + 1) >> &pArrayOfHeights, float maxHeight)
{
	// Set random height of corner vertices.
	(*pArrayOfHeights)[0] = -maxHeight + static_cast<float>(rand()) / (static_cast<float>
		(RAND_MAX / (maxHeight + maxHeight)));
	(*pArrayOfHeights)[NUMSQRS] = -maxHeight + static_cast<float>(rand()) / (static_cast<float>
		(RAND_MAX / (maxHeight + maxHeight)));
	(*pArrayOfHeights)[(NUMSQRS + 1) * (NUMSQRS + 1) - NUMSQRS - 1] = -maxHeight + static_cast<float>(rand()) / (static_cast<float>
		(RAND_MAX / (maxHeight + maxHeight)));
	(*pArrayOfHeights)[(NUMSQRS + 1) * (NUMSQRS + 1) - 1] = -maxHeight + static_cast<float>(rand()) / (static_cast<float>
		(RAND_MAX / (maxHeight + maxHeight)));

	size_t iterations = static_cast<size_t>(Math::Log2(NUMSQRS));
	size_t numberOfSquares = 1;
	size_t sizeOfSquare = NUMSQRS;

	for (size_t i = 0; i < iterations; ++i)
	{
		size_t row = 0;
		for (size_t j = 0; j < numberOfSquares; ++j)
		{
			size_t col = 0;
			for (size_t k = 0; k < numberOfSquares; ++k)
			{
				size_t halfSize = static_cast<size_t>(sizeOfSquare * 0.5f);
				size_t topLeft = static_cast<size_t>(row * (NUMSQRS + 1) + col);
				size_t bottomLeft = static_cast<size_t>((row + sizeOfSquare) * (NUMSQRS + 1) + col);

				// Diamond step.
				size_t middle = static_cast<size_t>((row + halfSize) * (NUMSQRS + 1)) + static_cast<size_t>(col + halfSize);
				(*pArrayOfHeights)[middle] = ((*pArrayOfHeights)[topLeft] + (*pArrayOfHeights)[topLeft + sizeOfSquare] +
					(*pArrayOfHeights)[bottomLeft] + (*pArrayOfHeights)[bottomLeft + sizeOfSquare]) * 0.25f +
					(-maxHeight + static_cast<float>(rand()) / (static_cast<float>
					(RAND_MAX / (maxHeight + maxHeight))));

				// Square step.
				(*pArrayOfHeights)[topLeft + halfSize] = ((*pArrayOfHeights)[topLeft] + (*pArrayOfHeights)[topLeft +
					sizeOfSquare] + (*pArrayOfHeights)[middle]) / 3 + (-maxHeight + static_cast<float>(rand()) / (static_cast<float>
					(RAND_MAX / (maxHeight + maxHeight))));
				(*pArrayOfHeights)[middle - halfSize] = ((*pArrayOfHeights)[topLeft] + (*pArrayOfHeights)[bottomLeft]
					+ (*pArrayOfHeights)[middle]) / 3 + (-maxHeight + static_cast<float>(rand()) / (static_cast<float>
					(RAND_MAX / (maxHeight + maxHeight))));
				(*pArrayOfHeights)[middle + halfSize] = ((*pArrayOfHeights)[topLeft + sizeOfSquare] + (*pArrayOfHeights)[bottomLeft + sizeOfSquare]
					+ (*pArrayOfHeights)[middle]) / 3 + (-maxHeight + static_cast<float>(rand()) / (static_cast<float>
					(RAND_MAX / (maxHeight + maxHeight))));
				(*pArrayOfHeights)[bottomLeft + halfSize] = ((*pArrayOfHeights)[bottomLeft] + (*pArrayOfHeights)[bottomLeft + sizeOfSquare]
					+ (*pArrayOfHeights)[middle]) / 3 + (-maxHeight + static_cast<float>(rand()) / (static_cast<float>
					(RAND_MAX / (maxHeight + maxHeight))));


				col += sizeOfSquare;
			}
			row += sizeOfSquare;
		}
		numberOfSquares *= 2;
		sizeOfSquare /= 2;
		maxHeight *= 0.5f;
	}


}


bool Renderer::keyPressed(const KeyboardEvent& evt)
{
	// Enumeration is in OgreInput.h at line 89
	switch (evt.keysym.sym)
	{
	case SDLK_ESCAPE:
		_endRendering = true;
		return true;

	case 'w': 
		_pCamNode->translate(_pCamNode->getLocalAxes(), 0, 0, -10);
		return true;

	case 's': 
		_pCamNode->translate(_pCamNode->getLocalAxes(), 0, 0, 10);
		return true;

	case 'd': 
		_pCamNode->translate(_pCamNode->getLocalAxes(), 10, 0, 0);
		return true;

	case 'a': 
		_pCamNode->translate(_pCamNode->getLocalAxes(), -10, 0, 0);
		return true;
	}
	return true;
}

bool Renderer::mouseWheelRolled(const MouseWheelEvent& evt) 
{
	// Movement in positive Z axis
	if (evt.y > 0)
	{
		_pCamNode->translate(_pCamNode->getLocalAxes(), 0, 10, 0);
		return true;
	}

	// Movement in negative Z axis
	if (evt.y < 0)
	{
		_pCamNode->translate(_pCamNode->getLocalAxes(), 0, -10, 0);
		return true;
	}
	return true;
}

bool Renderer::mousePressed(const MouseButtonEvent& evt)
{
	if (evt.button == ButtonType::BUTTON_LEFT)
	{
		_isLeftMousePressed = true;
		return true;
	}
	if (evt.button == ButtonType::BUTTON_RIGHT)
	{
		_isRightMousePressed = true;
		return true;
	}

	return true;
}
bool Renderer::mouseReleased(const MouseButtonEvent& evt)
{
	if (evt.button == ButtonType::BUTTON_LEFT)
	{
		_isLeftMousePressed = false;
		return true;
	}
	if (evt.button == ButtonType::BUTTON_RIGHT)
	{
		_isRightMousePressed = false;
		return true;
	}

	return true;
}


bool Renderer::mouseMoved(const MouseMotionEvent& evt)
{	
	if (_isLeftMousePressed)
	{
		_pCamNode->yaw(Radian(evt.xrel * _deltaTime * (-1)), Node::TS_LOCAL);
		_pCamNode->pitch(Radian(evt.yrel * _deltaTime * (-1)), Node::TS_LOCAL);
		
	}
	return true;
}

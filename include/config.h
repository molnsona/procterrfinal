#ifndef CONFIG_H_
#define CONFIG_H_

// Name of whole project.
#define NAME "ProcTerr"

// Set width and length of the terrain(must be square, in format (2^n + 1))
#define SIZE 4067

// Interval [-MAX_HEIGHT, MAX_HEIGHT] for generating random values of height
#define MAX_HEIGHT 1000.0f

// Number of squares per one line (must be in format (2^n) and <= 128)
#define NUMSQRS 128


// Number of triangles
#define NUMTR NUMSQRS * NUMSQRS * 2
#endif

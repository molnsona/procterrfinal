Need to install:
(ubuntu) sudo apt-get install libxt-dev libxaw7-dev zlib1g-dev libglu1-mesa-dev freeglut3-dev libsdl2-dev(this for movement)

(other) https://www.unixmen.com/how-to-install-ogre-in-linux/

UNIX:
mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
make install
cd bin
./ProcTerr

If in Debug mode, you need to change ${PATH_TO_BUILD_DIRECTORY}/dist/share/OGRE/plugins.cfg  file (after make install) to:

# Defines plugins to load

# Define plugin folder
PluginFolder=${PATH_TO_BUILD_DIRECTORY}/dist/lib/OGRE
	(e.g./home/devwesp/Desktop/ProcTerrSonicka/procterrfinal/build/dist/lib/OGRE)

# Define plugins
# Plugin=RenderSystem_Direct3D9_d
# Plugin=RenderSystem_Direct3D11_d
 Plugin=RenderSystem_GL_d
 Plugin=RenderSystem_GL3Plus_d
# Plugin=RenderSystem_GLES2_d
 Plugin=Plugin_ParticleFX_d
 Plugin=Plugin_BSPSceneManager_d
# Plugin=Plugin_CgProgramManager_d
# Plugin=Codec_EXR_d
 Plugin=Codec_STBI_d
# Plugin=Codec_FreeImage_d
 Plugin=Plugin_PCZSceneManager_d
 Plugin=Plugin_OctreeZone_d
 Plugin=Plugin_OctreeSceneManager_d

and run ./ProcTerr_d in ${PATH_TO_BUILD_DIRECTORY}/bin folder

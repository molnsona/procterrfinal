#ifndef RENDERER_H_
#define RENDERER_H_

#pragma warning(push,0)
#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#pragma warning(pop)

#include "config.h"

using namespace Ogre;
using namespace OgreBites;

namespace ProcTerr
{
	class Renderer: public ApplicationContext, public InputListener
	{
		SceneManager* pScnMngr;
		Camera* _pCam;
		SceneNode* _pCamNode;

		float _deltaTime;
		bool _endRendering;

		bool _isLeftMousePressed;
		bool _isRightMousePressed;
		Vector3 _lookPoint;

		std::unique_ptr<std::array<std::array<Vector2, NUMSQRS + 2>, NUMSQRS + 2>> _grid;

		void _DiamondSquare(std::unique_ptr<std::array<
			float, (NUMSQRS + 1) * (NUMSQRS + 1 )>> &pArrayOfHeights, float maxHeight);
		
		float _Perlin(float x, float y, float z);
		float _Grad(int hash, float x, float y, float z);
		float _Lerp(float a, float b, float x);
		float _OctavePerlin(float x, float y, int octaves, float persistence);

		float _GetRandomFloat(float from, float to) const;

		void _CreateMesh(MeshPtr & mesh, SubMesh* pSubMesh);
		void _CountNormals(const std::unique_ptr < std::array<
			float, (NUMSQRS + 1) * (NUMSQRS + 1) * 6>> & pVertexArray, 
			const std::unique_ptr < std::array<unsigned short, NUMTR * 3>>& pTriangleArray);
		
	public:

		Renderer();
		virtual ~Renderer();

		bool getEndRendering() const;
		float getDeltaTime() const;
		void setEndRendering(bool isEnd);
		void setDeltaTime(float delta);

		void setup();
		bool keyPressed(const KeyboardEvent& evt);
		bool mouseWheelRolled(const MouseWheelEvent& evt);
		bool mouseMoved(const MouseMotionEvent& evt);
		bool mousePressed(const MouseButtonEvent& evt);
		bool mouseReleased(const MouseButtonEvent& evt);
	};
};
#endif //#ifndef RENDERER_H_


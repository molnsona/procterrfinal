#pragma warning(push,4)
#include "Renderer.h"
#pragma warning(pop)
#include <iostream>
#include <chrono>

using namespace ProcTerr;

int main(int argc, char **argv)
{
	// Main loop is in OgreRoot.cpp at line 864 in startRendering() method.
	try
	{
		Renderer app;
		auto previousTime = std::chrono::high_resolution_clock::now();
		app.setEndRendering(false);
		app.initApp();

		while (!app.getEndRendering())
		{
			auto currentTime = std::chrono::high_resolution_clock::now();
			app.setDeltaTime(static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(
				currentTime - previousTime).count()) / 1000000.0f);
			app.getRoot()->renderOneFrame();
			previousTime = currentTime;
		}
		app.closeApp();
	}
	catch (const std::exception& e)
	{
		std::cerr << "Error occurred during execution: " << e.what() << '\n';
		return 1;
	}

	return 0;
}
